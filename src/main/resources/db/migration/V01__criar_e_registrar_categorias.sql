CREATE TABLE categoria(
	codigo INT IDENTITY(1,1) PRIMARY KEY,
	nome varchar(50) NOT NULL
);

INSERT INTO categoria(nome) values ('Lazer');
INSERT INTO categoria(nome) values ('Alimentação');
INSERT INTO categoria(nome) values ('Supermercado');
INSERT INTO categoria(nome) values ('Farmácia');
INSERT INTO categoria(nome) values ('Outros');
