CREATE TABLE pessoa(
	codigo INT IDENTITY(1,1) PRIMARY KEY,
	nome VARCHAR(50) NOT NULL,
	ativo BIT NOT NULL,
	logradouro VARCHAR(60),
	numero VARCHAR(10),
	complemento VARCHAR(20),
	bairro VARCHAR(20),
	cep VARCHAR(10),
	cidade VARCHAR(40),
	estado VARCHAR(10)
);

INSERT INTO [dbo].[pessoa]
           ([nome]
           ,[ativo]
           ,[logradouro]
           ,[numero]
           ,[complemento]
           ,[bairro]
           ,[cep]
           ,[cidade]
           ,[estado])
     VALUES
           ('Andre Henrique',1,'Rua Aurora','246','Sem','Vila','06321-234','Carapicuiba','São Paulo');
INSERT INTO [dbo].[pessoa]
           ([nome]
           ,[ativo]
           ,[logradouro]
           ,[numero]
           ,[complemento]
           ,[bairro]
           ,[cep]
           ,[cidade]
           ,[estado])
     VALUES
           ('Flavia Ap',1,'Rua Aurora','247','Sem','Vila','06321-311','Carapicuiba','São Paulo');
INSERT INTO [dbo].[pessoa]
           ([nome]
           ,[ativo]
           ,[logradouro]
           ,[numero]
           ,[complemento]
           ,[bairro]
           ,[cep]
           ,[cidade]
           ,[estado])
     VALUES
           ('Edson',1,'Rua Aurora','248','Sem','Vila','06321-312','Carapicuiba','São Paulo');
INSERT INTO [dbo].[pessoa]
           ([nome]
           ,[ativo]
           ,[logradouro]
           ,[numero]
           ,[complemento]
           ,[bairro]
           ,[cep]
           ,[cidade]
           ,[estado])
     VALUES
           ('Allana',0,'Rua Aurora','249','Sem','Vila','06321-313','Carapicuiba','São Paulo');
INSERT INTO [dbo].[pessoa]
           ([nome]
           ,[ativo]
           ,[logradouro]
           ,[numero]
           ,[complemento]
           ,[bairro]
           ,[cep]
           ,[cidade]
           ,[estado])
     VALUES
           ('João',0,'Avenida 10','57787','Casa 98','Vila Marcondes','06321-314','Carapicuiba','São Paulo');
INSERT INTO [dbo].[pessoa]
           ([nome]
           ,[ativo]
           ,[logradouro]
           ,[numero]
           ,[complemento]
           ,[bairro]
           ,[cep]
           ,[cidade]
           ,[estado])
     VALUES
           ('João Pedro',1,'Avenida 11','57788','Casa 99','Vila Marcondes','06321-315','Carapicuiba','São Paulo');
INSERT INTO [dbo].[pessoa]
           ([nome]
           ,[ativo]
           ,[logradouro]
           ,[numero]
           ,[complemento]
           ,[bairro]
           ,[cep]
           ,[cidade]
           ,[estado])
     VALUES
           ('João Paulo',2,'Avenida 12','57789','Casa 100','Vila Marcondes','06321-316','Carapicuiba','São Paulo');
INSERT INTO [dbo].[pessoa]
           ([nome]
           ,[ativo]
           ,[logradouro]
           ,[numero]
           ,[complemento]
           ,[bairro]
           ,[cep]
           ,[cidade]
           ,[estado])
     VALUES
           ('João Alvin',3,'Avenida 13','57790','Casa 101','Vila Marcondes','06321-317','Carapicuiba','São Paulo');
INSERT INTO [dbo].[pessoa]
           ([nome]
           ,[ativo]
           ,[logradouro]
           ,[numero]
           ,[complemento]
           ,[bairro]
           ,[cep]
           ,[cidade]
           ,[estado])
     VALUES
           ('João IV',4,'Avenida 14','57791','Casa 102','Vila Marcondes','06321-318','Carapicuiba','São Paulo');
INSERT INTO [dbo].[pessoa]
           ([nome]
           ,[ativo]
           ,[logradouro]
           ,[numero]
           ,[complemento]
           ,[bairro]
           ,[cep]
           ,[cidade]
           ,[estado])
     VALUES
           ('João V',5,'Avenida 15','57792','Casa 103','Vila Marcondes','06321-319','Carapicuiba','São Paulo');
INSERT INTO [dbo].[pessoa]
           ([nome]
           ,[ativo]
           ,[logradouro]
           ,[numero]
           ,[complemento]
           ,[bairro]
           ,[cep]
           ,[cidade]
           ,[estado])
     VALUES
           ('João VI',6,'Avenida 16','57793','Casa 104','Vila Marcondes','06321-320','Carapicuiba','São Paulo');
INSERT INTO [dbo].[pessoa]
           ([nome]
           ,[ativo]
           ,[logradouro]
           ,[numero]
           ,[complemento]
           ,[bairro]
           ,[cep]
           ,[cidade]
           ,[estado])
     VALUES
           ('João VII',7,'Avenida 17','57794','Casa 105','Vila Marcondes','06321-321','Carapicuiba','São Paulo');
INSERT INTO [dbo].[pessoa]
           ([nome]
           ,[ativo]
           ,[logradouro]
           ,[numero]
           ,[complemento]
           ,[bairro]
           ,[cep]
           ,[cidade]
           ,[estado])
     VALUES
           ('João VII',8,'Avenida 18','57795','Casa 106','Vila Marcondes','06321-322','Carapicuiba','São Paulo');
