package com.example.algamoney.api.lancamento;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import com.example.algamoney.api.pessoa.Pessoa;
import com.example.algamoney.api.pessoa.PessoaRepository;
import com.example.algamoney.api.pessoa.exception.PessoaInexistenteOuInativoException;

@Service
public class LancamentoService {

	@Autowired
	private PessoaRepository pessoaRepository;

	@Autowired
	private LancamentoRepository lancamentoRepository;

	public Lancamento salvar(Lancamento lancamento) {
		Pessoa pessoa = pessoaRepository.findOne(lancamento.getPessoa().getCodigo());
		if (pessoa == null || !pessoa.getAtivo()) {
			throw new PessoaInexistenteOuInativoException();
		}
		return lancamentoRepository.save(lancamento);
	}

	public void deletar(Long codigo) {
		Lancamento lancamento = lancamentoRepository.findOne(codigo);
		if (lancamento != null) {
			lancamentoRepository.delete(codigo);
		} else {
			throw new EmptyResultDataAccessException(1);
		}
	}

}
